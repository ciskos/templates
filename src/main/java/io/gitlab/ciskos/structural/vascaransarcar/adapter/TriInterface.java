package io.gitlab.ciskos.structural.vascaransarcar.adapter;

interface TriInterface {
	
	void aboutTriangle();
	double calculateTriangleArea();
}
