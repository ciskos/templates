package io.gitlab.ciskos.structural.vascaransarcar.adapter;

interface RectInterface {
	
	void aboutMe();
	double calculateArea();
}
