package io.gitlab.ciskos.structural.vascaransarcar.proxy.implementation_1.components;

// The abstract class Subject
public abstract class Subject {
	public abstract void doSomeWork();
}
