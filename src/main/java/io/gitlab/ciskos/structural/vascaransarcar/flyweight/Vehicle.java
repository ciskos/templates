package io.gitlab.ciskos.structural.vascaransarcar.flyweight;

interface Vehicle {
	// Color comes from client. It is extrinsic.
	void aboutMe(String color);
}
