package io.gitlab.ciskos.structural.refactoringguru.bridge.remotes;

import io.gitlab.ciskos.structural.refactoringguru.bridge.devices.Device;

public class AdvancedRemote extends BasicRemote {

    public AdvancedRemote(Device device) {
        super.device = device;
    }

    public void mute() {
        System.out.println("Remote: mute");
        device.setVolume(0);
    }
}
