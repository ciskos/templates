package io.gitlab.ciskos.structural.refactoringguru.bridge;

import io.gitlab.ciskos.structural.refactoringguru.bridge.devices.Device;
import io.gitlab.ciskos.structural.refactoringguru.bridge.devices.Radio;
import io.gitlab.ciskos.structural.refactoringguru.bridge.devices.Tv;
import io.gitlab.ciskos.structural.refactoringguru.bridge.remotes.AdvancedRemote;
import io.gitlab.ciskos.structural.refactoringguru.bridge.remotes.BasicRemote;

public class Demo {
    public static void main(String[] args) {
        testDevice(new Tv());
        testDevice(new Radio());
    }

    public static void testDevice(Device device) {
        System.out.println("Tests with basic remote.");
        BasicRemote basicRemote = new BasicRemote(device);
        basicRemote.power();
        device.printStatus();

        System.out.println("Tests with advanced remote.");
        AdvancedRemote advancedRemote = new AdvancedRemote(device);
        advancedRemote.power();
        advancedRemote.mute();
        device.printStatus();
    }
}
