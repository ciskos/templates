package io.gitlab.ciskos.creational.mine.factorymethod.factory;

import io.gitlab.ciskos.creational.mine.factorymethod.tv.Lcd;
import io.gitlab.ciskos.creational.mine.factorymethod.tv.Tv;

public class ConcreteLcdFactory implements Factory {

	@Override
	public Tv getTv() {
		return new Lcd();
	}

}
