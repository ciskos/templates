package io.gitlab.ciskos.creational.mine.abstractfactory;

public interface AbstractB {

	String addString();
	String removeString();
	
}
