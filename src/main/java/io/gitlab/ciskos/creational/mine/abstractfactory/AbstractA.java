package io.gitlab.ciskos.creational.mine.abstractfactory;

public interface AbstractA {

	String addString();
	String removeString();
	
}
