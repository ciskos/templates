package io.gitlab.ciskos.creational.mine.abstractfactory;

public class ConcreteFactoryOf1 implements AbstractFactory {

	public AbstractA createAbstractA() {
		return new ConcreteA1();
	}

	public AbstractB createAbstractB() {
		return new ConcreteB1();
	}

}
