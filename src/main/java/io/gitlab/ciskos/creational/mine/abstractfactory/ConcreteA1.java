package io.gitlab.ciskos.creational.mine.abstractfactory;

public class ConcreteA1 implements AbstractA {

	public String addString() {
		return "добавляем А1";
	}

	public String removeString() {
		return "удаляем А1";
	}

}
