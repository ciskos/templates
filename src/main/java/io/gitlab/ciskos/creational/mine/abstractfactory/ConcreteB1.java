package io.gitlab.ciskos.creational.mine.abstractfactory;

public class ConcreteB1 implements AbstractB {

	public String addString() {
		return "добавляем B1";
	}

	public String removeString() {
		return "удаляем B1";
	}

}
