package io.gitlab.ciskos.creational.mine.abstractfactory;

public class ConcreteFactoryOf2 implements AbstractFactory {

	public AbstractA createAbstractA() {
		return new ConcreteA2();
	}

	public AbstractB createAbstractB() {
		return new ConcreteB2();
	}

}
