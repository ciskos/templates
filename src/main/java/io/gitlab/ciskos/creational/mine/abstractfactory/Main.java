package io.gitlab.ciskos.creational.mine.abstractfactory;

public class Main {

	private AbstractFactory factory;
	
	public static void main(String[] args) {
		Main main = new Main();
		
		System.out.println("Concrete factory of 1");
		main.setFactory(new ConcreteFactoryOf1());
		
		System.out.println(main.factory.createAbstractA().addString());
		System.out.println(main.factory.createAbstractA().removeString());
		System.out.println(main.factory.createAbstractB().addString());
		System.out.println(main.factory.createAbstractB().removeString());
		
		System.out.println("\nConcrete factory of 2");
		main.setFactory(new ConcreteFactoryOf2());
		
		System.out.println(main.factory.createAbstractA().addString());
		System.out.println(main.factory.createAbstractA().removeString());
		System.out.println(main.factory.createAbstractB().addString());
		System.out.println(main.factory.createAbstractB().removeString());
		
	}

	public void setFactory(AbstractFactory factory) {
		this.factory = factory;
	}

}
