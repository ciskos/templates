package io.gitlab.ciskos.creational.mine.factorymethod.tv;

public class Lcd implements Tv {

	public void show() {
		System.out.println("lcd show");
	}

}
