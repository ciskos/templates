package io.gitlab.ciskos.creational.mine.abstractfactory;

public class ConcreteA2 implements AbstractA {

	public String addString() {
		return "добавляем А2";
	}

	public String removeString() {
		return "удаляем А2";
	}

}
