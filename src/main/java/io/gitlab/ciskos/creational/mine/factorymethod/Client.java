package io.gitlab.ciskos.creational.mine.factorymethod;

import io.gitlab.ciskos.creational.mine.factorymethod.factory.ConcreteLcdFactory;
import io.gitlab.ciskos.creational.mine.factorymethod.factory.ConcreteTubeFactory;
import io.gitlab.ciskos.creational.mine.factorymethod.factory.Factory;

public class Client {

	private static Factory factory;
	
	public static void main(String[] args) {
		System.out.print("now tube show - ");
		factory = new ConcreteTubeFactory();
		factory.createTv();
		
		System.out.print("now lcd show - ");
		factory = new ConcreteLcdFactory();
		factory.createTv();
	}

}
