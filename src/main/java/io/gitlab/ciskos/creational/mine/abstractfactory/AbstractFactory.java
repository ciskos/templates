package io.gitlab.ciskos.creational.mine.abstractfactory;

public interface AbstractFactory {

	AbstractA createAbstractA();
	AbstractB createAbstractB();
	
}
