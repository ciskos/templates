package io.gitlab.ciskos.creational.mine.factorymethod.tv;

public class Tube implements Tv {

	public void show() {
		System.out.println("tube show");
	}

}
