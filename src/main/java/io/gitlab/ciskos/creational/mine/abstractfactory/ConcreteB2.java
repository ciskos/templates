package io.gitlab.ciskos.creational.mine.abstractfactory;

public class ConcreteB2 implements AbstractB {

	public String addString() {
		return "добавляем B2";
	}

	public String removeString() {
		return "удаляем B2";
	}

}
