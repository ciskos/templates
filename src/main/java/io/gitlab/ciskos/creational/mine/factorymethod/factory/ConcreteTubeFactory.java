package io.gitlab.ciskos.creational.mine.factorymethod.factory;

import io.gitlab.ciskos.creational.mine.factorymethod.tv.Tube;
import io.gitlab.ciskos.creational.mine.factorymethod.tv.Tv;

public class ConcreteTubeFactory implements Factory {

	@Override
	public Tv getTv() {
		return new Tube();
	}

}
