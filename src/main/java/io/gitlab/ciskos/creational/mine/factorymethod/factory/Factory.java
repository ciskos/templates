package io.gitlab.ciskos.creational.mine.factorymethod.factory;

import io.gitlab.ciskos.creational.mine.factorymethod.tv.Tv;

public interface Factory {

	default void createTv() {
		Tv tv = getTv();
		tv.show();
	}
	
	/*
	 * Собственно фабричный метод.
	 * */
	Tv getTv();
}
