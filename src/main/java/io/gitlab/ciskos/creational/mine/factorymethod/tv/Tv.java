package io.gitlab.ciskos.creational.mine.factorymethod.tv;

public interface Tv {

	void show();
	
}
