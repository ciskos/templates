package io.gitlab.ciskos.creational.refactoringguru.abstractfactory.factories;

import io.gitlab.ciskos.creational.refactoringguru.abstractfactory.buttons.Button;
import io.gitlab.ciskos.creational.refactoringguru.abstractfactory.buttons.MacOSButton;
import io.gitlab.ciskos.creational.refactoringguru.abstractfactory.checkboxes.Checkbox;
import io.gitlab.ciskos.creational.refactoringguru.abstractfactory.checkboxes.MacOSCheckbox;

/**
 * Каждая конкретная фабрика знает и создаёт только продукты своей вариации.
 */
public class MacOSFactory implements GUIFactory {

    @Override
    public Button createButton() {
        return new MacOSButton();
    }

    @Override
    public Checkbox createCheckbox() {
        return new MacOSCheckbox();
    }
}