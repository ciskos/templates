package io.gitlab.ciskos.creational.refactoringguru.builder.cars;

public enum CarType {
    CITY_CAR, SPORTS_CAR, SUV
}
