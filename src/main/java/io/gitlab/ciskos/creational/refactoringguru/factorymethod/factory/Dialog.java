package io.gitlab.ciskos.creational.refactoringguru.factorymethod.factory;

import io.gitlab.ciskos.creational.refactoringguru.factorymethod.buttons.Button;

/**
 * RU: Базовый класс фабрики. Заметьте, что "фабрика" – это всего лишь
 * дополнительная роль для класса. Он уже имеет какую-то бизнес-логику, в
 * которой требуется создание разнообразных продуктов.
 */
public abstract class Dialog {

    public void renderWindow() {
        // RU: ... остальной код диалога ...

        Button okButton = createButton();
        okButton.render();
    }

    /**
     * RU: Подклассы будут переопределять этот метод, чтобы создавать конкретные
     * объекты продуктов, разные для каждой фабрики.
     */
    public abstract Button createButton();

}
