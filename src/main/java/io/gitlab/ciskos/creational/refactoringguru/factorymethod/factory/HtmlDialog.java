package io.gitlab.ciskos.creational.refactoringguru.factorymethod.factory;

import io.gitlab.ciskos.creational.refactoringguru.factorymethod.buttons.Button;
import io.gitlab.ciskos.creational.refactoringguru.factorymethod.buttons.HtmlButton;

/**
 * RU: HTML-диалог.
 */
public class HtmlDialog extends Dialog {

    @Override
    public Button createButton() {
        return new HtmlButton();
    }

}
