package io.gitlab.ciskos.creational.refactoringguru.abstractfactory.factories;

import io.gitlab.ciskos.creational.refactoringguru.abstractfactory.buttons.Button;
import io.gitlab.ciskos.creational.refactoringguru.abstractfactory.buttons.WindowsButton;
import io.gitlab.ciskos.creational.refactoringguru.abstractfactory.checkboxes.Checkbox;
import io.gitlab.ciskos.creational.refactoringguru.abstractfactory.checkboxes.WindowsCheckbox;

/**
 * Каждая конкретная фабрика знает и создаёт только продукты своей вариации.
 */
public class WindowsFactory implements GUIFactory {

    @Override
    public Button createButton() {
        return new WindowsButton();
    }

    @Override
    public Checkbox createCheckbox() {
        return new WindowsCheckbox();
    }
}