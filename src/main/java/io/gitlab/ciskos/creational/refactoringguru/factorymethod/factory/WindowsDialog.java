package io.gitlab.ciskos.creational.refactoringguru.factorymethod.factory;

import io.gitlab.ciskos.creational.refactoringguru.factorymethod.buttons.Button;
import io.gitlab.ciskos.creational.refactoringguru.factorymethod.buttons.WindowsButton;

/**
 * RU: Диалог на элементах операционной системы.
 */
public class WindowsDialog extends Dialog {

    @Override
    public Button createButton() {
        return new WindowsButton();
    }

}
