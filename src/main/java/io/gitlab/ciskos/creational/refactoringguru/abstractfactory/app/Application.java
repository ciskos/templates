package io.gitlab.ciskos.creational.refactoringguru.abstractfactory.app;

import io.gitlab.ciskos.creational.refactoringguru.abstractfactory.buttons.Button;
import io.gitlab.ciskos.creational.refactoringguru.abstractfactory.checkboxes.Checkbox;
import io.gitlab.ciskos.creational.refactoringguru.abstractfactory.factories.GUIFactory;

/**
 * Код, использующий фабрику, не волнует с какой конкретно фабрикой он работает.
 * Все получатели продуктов работают с продуктами через абстрактный интерфейс.
 */
public class Application {
    private Button button;
    private Checkbox checkbox;

    public Application(GUIFactory factory) {
        button = factory.createButton();
        checkbox = factory.createCheckbox();
    }

    public void paint() {
        button.paint();
        checkbox.paint();
    }
}