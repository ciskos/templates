package io.gitlab.ciskos.creational.refactoringguru.builder.components;

/**
 * Одна из фишек автомобиля.
 */
public enum Transmission {
    SINGLE_SPEED, MANUAL, AUTOMATIC, SEMI_AUTOMATIC
}
