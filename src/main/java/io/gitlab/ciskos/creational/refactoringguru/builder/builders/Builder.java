package io.gitlab.ciskos.creational.refactoringguru.builder.builders;

import io.gitlab.ciskos.creational.refactoringguru.builder.cars.CarType;
import io.gitlab.ciskos.creational.refactoringguru.builder.components.Engine;
import io.gitlab.ciskos.creational.refactoringguru.builder.components.GPSNavigator;
import io.gitlab.ciskos.creational.refactoringguru.builder.components.Transmission;
import io.gitlab.ciskos.creational.refactoringguru.builder.components.TripComputer;

/**
 * Интерфейс Строителя объявляет все возможные этапы и шаги конфигурации
 * продукта.
 */
public interface Builder {
    void setCarType(CarType type);
    void setSeats(int seats);
    void setEngine(Engine engine);
    void setTransmission(Transmission transmission);
    void setTripComputer(TripComputer tripComputer);
    void setGPSNavigator(GPSNavigator gpsNavigator);
}
