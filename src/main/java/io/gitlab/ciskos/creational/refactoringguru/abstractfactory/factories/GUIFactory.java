package io.gitlab.ciskos.creational.refactoringguru.abstractfactory.factories;

import io.gitlab.ciskos.creational.refactoringguru.abstractfactory.buttons.Button;
import io.gitlab.ciskos.creational.refactoringguru.abstractfactory.checkboxes.Checkbox;

/**
 * Абстрактная фабрика знает обо всех (абстрактных) типах продуктов.
 */
public interface GUIFactory {
    Button createButton();
    Checkbox createCheckbox();
}