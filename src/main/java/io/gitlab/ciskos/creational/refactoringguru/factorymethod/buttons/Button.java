package io.gitlab.ciskos.creational.refactoringguru.factorymethod.buttons;

/**
 * RU: Общий интерфейс для всех продуктов.
 */
public interface Button {

    void render();
    void onClick();

}
