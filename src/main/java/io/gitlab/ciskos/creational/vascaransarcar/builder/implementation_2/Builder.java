package io.gitlab.ciskos.creational.vascaransarcar.builder.implementation_2;

// The common interface
interface Builder {
	
	Builder addBrandName();

	Builder buildBody();

	Builder insertWheels();	

	// The following method is used to
	// retrieve the object that is constructed.	 
	Vehicle getVehicle();
}
