package io.gitlab.ciskos.creational.vascaransarcar.abstractfactory;

//Abstract Product-1
public interface Tiger
{
  void aboutMe();
  void inviteDog(Dog dog);
}

