package io.gitlab.ciskos.creational.vascaransarcar.abstractfactory.pet;


import io.gitlab.ciskos.creational.vascaransarcar.abstractfactory.AnimalFactory;
import io.gitlab.ciskos.creational.vascaransarcar.abstractfactory.Dog;
import io.gitlab.ciskos.creational.vascaransarcar.abstractfactory.Tiger;

// Concrete Factory2: Pet animal factory
public class PetAnimalFactory extends AnimalFactory {
	public PetAnimalFactory() {
		System.out.println("You opt for a pet animal factory.\n");
	}

	@Override
	public Tiger createTiger(String color) {
		return new PetTiger(color);
	}

	@Override
	public Dog createDog(String color) {
		return new PetDog(color);
	}
}