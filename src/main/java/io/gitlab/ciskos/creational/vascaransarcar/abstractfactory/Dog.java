package io.gitlab.ciskos.creational.vascaransarcar.abstractfactory;

//Abstract Product-2
public interface Dog
{
 void displayMe();
}