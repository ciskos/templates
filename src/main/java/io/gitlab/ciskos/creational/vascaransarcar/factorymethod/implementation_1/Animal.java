package io.gitlab.ciskos.creational.vascaransarcar.factorymethod.implementation_1;

interface Animal{
    void displayBehavior();
}
