package io.gitlab.ciskos.creational.vascaransarcar.factorymethod.implementation_2;

interface Animal{
    void displayBehavior();
}

