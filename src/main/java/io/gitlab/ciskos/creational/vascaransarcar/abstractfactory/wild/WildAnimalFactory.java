package io.gitlab.ciskos.creational.vascaransarcar.abstractfactory.wild;

import io.gitlab.ciskos.creational.vascaransarcar.abstractfactory.AnimalFactory;
import io.gitlab.ciskos.creational.vascaransarcar.abstractfactory.Dog;
import io.gitlab.ciskos.creational.vascaransarcar.abstractfactory.Tiger;

//Concrete Factory 1: Wild animal factory
public class WildAnimalFactory extends AnimalFactory {
	public WildAnimalFactory() {
		System.out.println("You opt for a wild animal factory.\n");
	}

	@Override
	public Tiger createTiger(String color) {
		return new WildTiger(color);
	}

	@Override
	public Dog createDog(String color) {
		return new WildDog(color);
	}
}