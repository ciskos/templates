package io.gitlab.ciskos.behavioral.refactoringguru.mediator.components;

import java.awt.event.ActionEvent;

import javax.swing.JButton;

import io.gitlab.ciskos.behavioral.refactoringguru.mediator.mediator.Mediator;
import io.gitlab.ciskos.behavioral.refactoringguru.mediator.mediator.Note;

/**
 * Конкретные компоненты никак не связаны между собой. У них есть только один
 * канал общения – через отправку уведомлений посреднику.
 */
public class AddButton extends JButton implements Component {
    private Mediator mediator;

    public AddButton() {
        super("Add");
    }

    @Override
    public void setMediator(Mediator mediator) {
        this.mediator = mediator;
    }

    @Override
    protected void fireActionPerformed(ActionEvent actionEvent) {
        mediator.addNewNote(new Note());
    }

    @Override
    public String getName() {
        return "AddButton";
    }
}
