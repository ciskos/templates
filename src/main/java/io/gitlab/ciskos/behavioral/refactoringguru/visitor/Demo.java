package io.gitlab.ciskos.behavioral.refactoringguru.visitor;

import io.gitlab.ciskos.behavioral.refactoringguru.visitor.shapes.Dot;
import io.gitlab.ciskos.behavioral.refactoringguru.visitor.shapes.Circle;
import io.gitlab.ciskos.behavioral.refactoringguru.visitor.shapes.CompoundShape;
import io.gitlab.ciskos.behavioral.refactoringguru.visitor.shapes.Rectangle;
import io.gitlab.ciskos.behavioral.refactoringguru.visitor.shapes.Shape;
import io.gitlab.ciskos.behavioral.refactoringguru.visitor.visitor.XMLExportVisitor;

public class Demo {
    public static void main(String[] args) {
        Dot dot = new Dot(1, 10, 55);
        Circle circle = new Circle(2, 23, 15, 10);
        Rectangle rectangle = new Rectangle(3, 10, 17, 20, 30);

        CompoundShape compoundShape = new CompoundShape(4);
        compoundShape.add(dot);
        compoundShape.add(circle);
        compoundShape.add(rectangle);

        CompoundShape c = new CompoundShape(5);
        c.add(dot);
        compoundShape.add(c);

        export(circle, compoundShape);
    }

    private static void export(Shape... shapes) {
        XMLExportVisitor exportVisitor = new XMLExportVisitor();
        System.out.println(exportVisitor.export(shapes));
    }
}
