package io.gitlab.ciskos.behavioral.refactoringguru.memento.commands;

public interface Command {
    String getName();
    void execute();
}
