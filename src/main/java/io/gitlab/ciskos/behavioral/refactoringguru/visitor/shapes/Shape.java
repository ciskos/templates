package io.gitlab.ciskos.behavioral.refactoringguru.visitor.shapes;

import io.gitlab.ciskos.behavioral.refactoringguru.visitor.visitor.Visitor;

public interface Shape {
    void move(int x, int y);
    void draw();
    String accept(Visitor visitor);
}
