package io.gitlab.ciskos.behavioral.refactoringguru.mediator.mediator;

import javax.swing.ListModel;

import io.gitlab.ciskos.behavioral.refactoringguru.mediator.components.Component;

/**
 * Общий интерфейс посредников.
 */
public interface Mediator {
    void addNewNote(Note note);
    void deleteNote();
    void getInfoFromList(Note note);
    void saveChanges();
    void markNote();
    void clear();
    void sendToFilter(ListModel listModel);
    void setElementsList(ListModel list);
    void registerComponent(Component component);
    void hideElements(boolean flag);
    void createGUI();
}
