package io.gitlab.ciskos.behavioral.refactoringguru.visitor.visitor;

import io.gitlab.ciskos.behavioral.refactoringguru.visitor.shapes.Dot;
import io.gitlab.ciskos.behavioral.refactoringguru.visitor.shapes.Circle;
import io.gitlab.ciskos.behavioral.refactoringguru.visitor.shapes.CompoundShape;
import io.gitlab.ciskos.behavioral.refactoringguru.visitor.shapes.Rectangle;

public interface Visitor {
	String visitDot(Dot dot);

    String visitCircle(Circle circle);

    String visitRectangle(Rectangle rectangle);

    String visitCompoundGraphic(CompoundShape cg);
}
