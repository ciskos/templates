package io.gitlab.ciskos.behavioral.refactoringguru.mediator;

import javax.swing.DefaultListModel;

import io.gitlab.ciskos.behavioral.refactoringguru.mediator.components.AddButton;
import io.gitlab.ciskos.behavioral.refactoringguru.mediator.components.DeleteButton;
import io.gitlab.ciskos.behavioral.refactoringguru.mediator.components.Filter;
import io.gitlab.ciskos.behavioral.refactoringguru.mediator.components.List;
import io.gitlab.ciskos.behavioral.refactoringguru.mediator.components.SaveButton;
import io.gitlab.ciskos.behavioral.refactoringguru.mediator.components.TextBox;
import io.gitlab.ciskos.behavioral.refactoringguru.mediator.components.Title;
import io.gitlab.ciskos.behavioral.refactoringguru.mediator.mediator.Editor;
import io.gitlab.ciskos.behavioral.refactoringguru.mediator.mediator.Mediator;

/**
 * Демо-класс. Здесь всё сводится воедино.
 */
public class Demo {
    public static void main(String[] args) {
        Mediator mediator = new Editor();

        mediator.registerComponent(new Title());
        mediator.registerComponent(new TextBox());
        mediator.registerComponent(new AddButton());
        mediator.registerComponent(new DeleteButton());
        mediator.registerComponent(new SaveButton());
        mediator.registerComponent(new List(new DefaultListModel()));
        mediator.registerComponent(new Filter());

        mediator.createGUI();
    }
}
