package io.gitlab.ciskos.behavioral.refactoringguru.iterator.iterators;

import io.gitlab.ciskos.behavioral.refactoringguru.iterator.profile.Profile;

public interface ProfileIterator {
    boolean hasNext();

    Profile getNext();

    void reset();
}