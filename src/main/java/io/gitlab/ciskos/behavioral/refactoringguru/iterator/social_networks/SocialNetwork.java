package io.gitlab.ciskos.behavioral.refactoringguru.iterator.social_networks;

import io.gitlab.ciskos.behavioral.refactoringguru.iterator.iterators.ProfileIterator;

public interface SocialNetwork {
    ProfileIterator createFriendsIterator(String profileEmail);

    ProfileIterator createCoworkersIterator(String profileEmail);
}
