package io.gitlab.ciskos.behavioral.refactoringguru.state;

import io.gitlab.ciskos.behavioral.refactoringguru.state.ui.Player;
import io.gitlab.ciskos.behavioral.refactoringguru.state.ui.UI;

/**
 * Демо-класс. Здесь всё сводится воедино.
 */
public class Demo {
    public static void main(String[] args) {
        Player player = new Player();
        UI ui = new UI(player);
        ui.init();
    }
}
