package io.gitlab.ciskos.behavioral.refactoringguru.mediator.components;

import io.gitlab.ciskos.behavioral.refactoringguru.mediator.mediator.Mediator;

/**
 * Общий интерфейс компонентов.
 */
public interface Component {
    void setMediator(Mediator mediator);
    String getName();
}
