package io.gitlab.ciskos.behavioral.refactoringguru.mediator.components;

import java.awt.event.KeyEvent;

import javax.swing.JTextField;

import io.gitlab.ciskos.behavioral.refactoringguru.mediator.mediator.Mediator;

/**
 * Конкретные компоненты никак не связаны между собой. У них есть только один
 * канал общения – через отправку уведомлений посреднику.
 */
public class Title extends JTextField implements Component {
    private Mediator mediator;

    @Override
    public void setMediator(Mediator mediator) {
        this.mediator = mediator;
    }

    @Override
    protected void processComponentKeyEvent(KeyEvent keyEvent) {
        mediator.markNote();
    }

    @Override
    public String getName() {
        return "Title";
    }
}
