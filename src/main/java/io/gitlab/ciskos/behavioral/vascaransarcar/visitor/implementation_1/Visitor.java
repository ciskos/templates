package io.gitlab.ciskos.behavioral.vascaransarcar.visitor.implementation_1;

interface Visitor {
	// The method to visit the IntegerProcessor.
	void visitNumber(IntegerProcessor myInt);
}