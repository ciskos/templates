package io.gitlab.ciskos.behavioral.vascaransarcar.iterator.implementation_3;

import java.util.Iterator;

interface Database {
	Iterator<Employee> createIterator();
}
