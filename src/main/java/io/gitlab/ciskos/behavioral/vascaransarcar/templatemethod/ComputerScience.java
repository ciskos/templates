package io.gitlab.ciskos.behavioral.vascaransarcar.templatemethod;

// The concrete derived class: ComputerScience

class ComputerScience extends BasicEngineering {
	
	@Override
	public void courseOnSpecialPaper() {
		System.out.println("3. Object-Oriented Programming");
	}
}