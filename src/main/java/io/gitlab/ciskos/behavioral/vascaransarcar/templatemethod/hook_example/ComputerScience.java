package io.gitlab.ciskos.behavioral.vascaransarcar.templatemethod.hook_example;

// The concrete derived class: ComputerScience

class ComputerScience extends BasicEngineering {
	
	@Override
	public void courseOnSpecialPaper() {
		System.out.println("3. Object-Oriented Programming");
	}
}
