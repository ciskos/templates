package io.gitlab.ciskos.behavioral.vascaransarcar.observer.implementation_2;


interface Observer {
	void getNotification(Company company);

	void registerTo(Company company);

	void unregisterFrom(Company company);

	String getObserverName();
}
