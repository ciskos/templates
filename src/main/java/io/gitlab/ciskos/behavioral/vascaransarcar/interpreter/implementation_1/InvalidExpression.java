package io.gitlab.ciskos.behavioral.vascaransarcar.interpreter.implementation_1;


class InvalidExpression implements Employee {

	@Override
	public boolean interpret(Context context) {
		return false; // result is always false
	}
}
