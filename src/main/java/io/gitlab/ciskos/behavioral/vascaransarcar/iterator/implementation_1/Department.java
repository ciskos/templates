package io.gitlab.ciskos.behavioral.vascaransarcar.iterator.implementation_1;

interface Department {
	
	Iterator createIterator();

}
