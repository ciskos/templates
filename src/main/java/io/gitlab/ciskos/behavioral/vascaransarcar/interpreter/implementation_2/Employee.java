package io.gitlab.ciskos.behavioral.vascaransarcar.interpreter.implementation_2;

interface Employee {
	public boolean interpret(Context context);
}