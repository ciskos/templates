package io.gitlab.ciskos.behavioral.vascaransarcar.visitor.implementation_2;

interface Visitor {
	void visitTheElement(SeniorEmployee employees);
	void visitTheElement(JuniorEmployee employee);	
}
