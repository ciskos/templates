package io.gitlab.ciskos.behavioral.vascaransarcar.observer.implementation_1;

interface Observer {
	void getNotification(Company company);	
	String getObserverName();
}
