package io.gitlab.ciskos.behavioral.vascaransarcar.interpreter.implementation_1;

interface Employee {
	public boolean interpret(Context context);
}
