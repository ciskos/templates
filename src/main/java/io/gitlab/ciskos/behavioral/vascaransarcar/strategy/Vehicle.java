package io.gitlab.ciskos.behavioral.vascaransarcar.strategy;

// The Vehicle class

class Vehicle {

	String vehicleType;

	public Vehicle(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getVehicle() {
		return vehicleType;
	}
}
