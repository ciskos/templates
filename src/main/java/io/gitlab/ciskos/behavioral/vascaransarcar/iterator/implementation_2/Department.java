package io.gitlab.ciskos.behavioral.vascaransarcar.iterator.implementation_2;

import java.util.Iterator;

interface Department {
	Iterator<String> createIterator();	
}
