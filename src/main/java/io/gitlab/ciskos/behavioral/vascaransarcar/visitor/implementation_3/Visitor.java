package io.gitlab.ciskos.behavioral.vascaransarcar.visitor.implementation_3;

interface Visitor {
	void visitTheElement(SeniorEmployee employees);
	void visitTheElement(JuniorEmployee employee);	
}

