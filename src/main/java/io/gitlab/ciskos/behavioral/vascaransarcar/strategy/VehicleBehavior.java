package io.gitlab.ciskos.behavioral.vascaransarcar.strategy;

// Abstract Behavior

interface VehicleBehavior {
	void showDetail(Vehicle vehicle);
}
