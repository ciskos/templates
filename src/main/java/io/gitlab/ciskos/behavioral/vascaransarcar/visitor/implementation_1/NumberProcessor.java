package io.gitlab.ciskos.behavioral.vascaransarcar.visitor.implementation_1;

interface NumberProcessor {	
	void acceptVisitor(Visitor visitor);
}
