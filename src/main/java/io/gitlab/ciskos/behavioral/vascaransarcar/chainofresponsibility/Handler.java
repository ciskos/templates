package io.gitlab.ciskos.behavioral.vascaransarcar.chainofresponsibility;

interface Handler {
	void handleMessage(Message message);
	void nextErrorHandler(Handler nextHandler);
}